import cache.Cache;
import cache.LFUCashe;
import cache.LRUCache;

/**
 * @author Andrei Dziubiankou
 *
 */
public final class Runner {

    private Runner() {
        //don't to be used
    }

    /**
     * @param args for console
     */
    public static void main(final String[] args) {
        System.out.println("demonstration of LRUCache");
        String[] keys = {"A", "B", "C", "D", "E", "F"};
        Integer[] values = {5, 7, 9, 11, 13, 15};
        Cache<String, Integer> cache = new LRUCache<String, Integer>(5);
        for (int i = 0; i < keys.length; i++) {
            cache.put(keys[i], values[i]);
            printCasheInfo(cache);
            /*if (i == 3) {
                cache.get(keys[0]);     //for test
            }*/
        }

        System.out.println("demostration of LFUCache");
        cache = new LFUCashe<String, Integer>(5);
        for (int i = 0; i < keys.length; i++) {
            cache.put(keys[i], values[i]);
            if (i == 3) {
                continue;               //for test
            }
            cache.get(keys[i]);
            printCasheInfo(cache);
        }
    }

    private static void printCasheInfo(final Cache cache) {
        // TODO Auto-generated method stub
        System.out.println(cache);
        System.out.println(cache.size());
    }

}
