package cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Andrei Dziubiankou
 *
 * @param <K> type for key
 * @param <V> type for value
 */
public class LRUCache<K, V> implements Cache<K, V> {
    private final LinkedHashMap<K, V> map;

    /**
     * @param maxSize size of cashe
     */
    @SuppressWarnings("serial")
    public LRUCache(final int maxSize) {
        super();
        map = new LinkedHashMap<K, V>(maxSize, (float) 0.75, true) {
            private final int maxEntries = maxSize;
            protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
                return size() > maxEntries;
            }
        };
    }

    @Override
    public synchronized void put(final K key, final V value) {
        // TODO Auto-generated method stub
        map.put(key, value);
    }

    @Override
    public V get(final K key) {
        // TODO Auto-generated method stub
        if (map.containsKey(key)) {
            return map.get(key);
        } else {
            return null;
        }
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return map.toString();
    }

}
