package cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Andrei Dziubiankou
 *
 * @param <K> type for key
 * @param <V> type for value
 */
public class LFUCashe<K, V> implements Cache<K, V> {
    private final LinkedHashMap<K, CacheEntry<V>> map;
    private final int maxEntries;

    /**
     * @param maxSize size of cashe
     */

    private final class CacheEntry<T> {
        private T data;
        private int frequency;

        CacheEntry() { }

        public T getData() {
            return data;
        }
        public void setData(final T data) {
            this.data = data;
        }

        public int getFrequency() {
            return frequency;
        }
        public void setFrequency(final int frequency) {
            this.frequency = frequency;
        }

        @Override
        public String toString() {
            // TODO Auto-generated method stub
            return " frequency=" + getFrequency() + ";value=" + getData();
        }
    }

    /**
     * @param maxSize of cache
     */
    public LFUCashe(final int maxSize) {
        // TODO Auto-generated constructor stub
        maxEntries = maxSize;
        map = new LinkedHashMap<K, CacheEntry<V>>(maxSize);
    }

    @Override
    public synchronized void put(final K key, final V value) {
        // TODO Auto-generated method stub
        if (isFull()) {
            K entryKeyToBeRemoved = getLFURemovedKey();
            map.remove(entryKeyToBeRemoved);
        }
        CacheEntry<V> temp = new CacheEntry<V>();
        temp.setData(value);
        temp.setFrequency(0);
        map.put(key, temp);
    }

    @Override
    public V get(final K key) {
        // TODO Auto-generated method stub
        if (map.containsKey(key)) {
            CacheEntry<V> temp = map.get(key);
            int frequency = temp.getFrequency();
            frequency++;
            temp.setFrequency(frequency);
            return temp.getData();
        } else {
            return null;
        }
    }

    private boolean isFull() {
        if (size() == maxEntries) {
            return true;
        }
        return false;
    }

    private K getLFURemovedKey() {
        K key = null;
        int minFreq = Integer.MAX_VALUE;

        for (Map.Entry<K, CacheEntry<V>> entry : map.entrySet()) {
            if (minFreq > entry.getValue().frequency) {
                key = entry.getKey();
                minFreq = entry.getValue().frequency;
            }
        }

        return key;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return map.size();
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return map.toString();
    }

}
